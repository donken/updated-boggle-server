using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Net.Sockets;
using System.Threading;
using System.Net;
using CustomNetworking;
using System.Timers;
using BB;
using MySql.Data.MySqlClient;

namespace ServerUtilities
{
	public class BoggleServer
	{
		/// <summary>
		/// Representation Invariant
		/// - seconds and letters are provided by the user and are consistent for all games played on a single gameServer
		/// - pendingPlayer determines whether a game is waiting on player2 to connect
		/// - legalWords contains a list of all possible legal words
		/// - gameServer accepts sockets on port 2000 and administers them to string sockets as clients connect
		/// - gameStringSocket will always be the socket given to the player who next connects, and will be created with a new socket once it has been given away
		/// - closeServer will tell an updater method to close the gameServer
		/// - currentGame is forgotten once two players have connected and reassigned when a new game is requested by a third, fifth, seventh, etc... player
		/// </summary>
		private string seconds, letters;
		private bool pendingPlayer = false;
		private Dictionary<string, bool> legalWords;
		private TcpListener gameServer;
		private StringSocket gameStringSocket;
        private TcpListener webServer;
		private bool closeServer = false;
		private Game currentGame;
        public const String connectionString = "server=atr.eng.utah.edu;database=leman;uid=leman;password=00731679";
        private object lockObject = new object();

		/// <summary>
		/// Two parameter constructor containing a time and filepath to a dictionary
		/// </summary>
		/// <param name="seconds"></param>
		/// <param name="filePath"></param>
		public BoggleServer(string seconds, string filePath)
		{
			setUpServer(seconds, filePath, "");
		}
		
		/// <summary>
		/// Three parameter constructor containing a time, filepath to a dictonary, and set of letters to use as a board
		/// </summary>
		/// <param name="seconds"></param>
		/// <param name="filePath"></param>
		/// <param name="letters"></param>
		public BoggleServer(string seconds, string filePath, string letters)
		{
			setUpServer(seconds, filePath, letters);
		}
		
		/// <summary>
		/// Sets up the gameServer with the parameters provided by the constructor
		/// </summary>
		/// <param name="seconds"></param>
		/// <param name="filePath"></param>
		/// <param name="letters"></param>
		private void setUpServer(string seconds, string filePath, string letters)
		{
			legalWords = new Dictionary<string, bool>();
			
			this.seconds = seconds;
			this.letters = letters;
			
			loadDictionary(filePath);
			
			gameServer = new TcpListener(IPAddress.Any, 2000);
			gameServer.Start();
			gameServer.BeginAcceptSocket(socketAccepted, null);

            webServer = new TcpListener(IPAddress.Any, 2500);
            webServer.Start();
            new Thread(f =>
            {
                while (!closeServer)
                {
                    StringSocket webSocket = new StringSocket(webServer.AcceptSocket(), new UTF8Encoding());
                    webSocket.BeginReceive((s, e, o) => 
                    {
                        if (s == null) return;
                        s = s.Trim('\r');

                        string[] splitCommand = s.Split(new char[] { ' ' }, StringSplitOptions.RemoveEmptyEntries);

                        if (splitCommand.Length != 3 || splitCommand[0] != "GET" || splitCommand[2] != "HTTP/1.1") return;

                        using (MySqlConnection conn = new MySqlConnection(connectionString))
                        {
                            if (splitCommand[1] == "/players")
                                requestPlayers(conn, webSocket);
                            else if (splitCommand[1].Length > 15 && splitCommand[1].Substring(0, 14) == "/games?player=")
                                requestOnePlayer(conn, splitCommand[1].Substring(14), webSocket);
                            else if (splitCommand[1].Length > 9 && splitCommand[1].Substring(0, 9) == "/game?id=")
                                requestGame(conn, splitCommand[1].Substring(9), webSocket);
                        }
                    }, webSocket);
                }

            }).Start();
		}
		/// <summary>
		/// This method is called whenever a new socket is accepted
		/// If the gameServer is told to close, this is where it is closed
		/// </summary>
		/// <param name="a"></param>
		private void socketAccepted(IAsyncResult a)
		{
			if (closeServer)
			{
				gameServer.EndAcceptSocket(a);
				gameServer.Stop();
				if (gameStringSocket != null) gameStringSocket.closeStringSocket();
				return;
			}
			gameStringSocket = new StringSocket(gameServer.EndAcceptSocket(a), new UTF8Encoding());
			gameStringSocket.BeginReceive(ConnectionEstablished, gameStringSocket);
			gameServer.BeginAcceptSocket(socketAccepted, null);
		}

        //private void webSocketAccepted(IAsyncResult a)
        //{
        //    if (closeServer)
        //    {
        //        webServer.EndAcceptSocket(a);
        //        webServer.Stop();
        //        if (webStringSocket != null) webStringSocket.closeStringSocket();
        //        return;
        //    }
        //    webStringSocket = new StringSocket(webServer.EndAcceptSocket(a), new UTF8Encoding());
        //    webStringSocket.BeginReceive(WebConnectionEstablished, webStringSocket);
        //}
		
		/// <summary>
		/// Called once the current string socket has been connected. If it is a player1 connecting, then the game is created anew. If it is a player2 connecting, then pendingPlayer becomes false and the game is forgotten by the gameServer
		/// </summary>
		/// <param name="s"></param>
		/// <param name="e"></param>
		/// <param name="payload"></param>
		private void ConnectionEstablished(string s, Exception e, object payload)
		{
			if (!pendingPlayer)
			{
				pendingPlayer = true;
				currentGame = new Game(this.seconds, this.letters, this.legalWords);
				currentGame.addPlayer(s, payload as StringSocket);
			}
			else
			{
				currentGame.addPlayer(s, payload as StringSocket);
				pendingPlayer = false;
			}
		}

        private void WebConnectionEstablished(string s, Exception e, object payload)
        {
            
        }

        private void requestPlayers(MySqlConnection conn, StringSocket ws)
        {
            StringBuilder s = new StringBuilder("HTTP/1.1 200 OK\r\nConnection: close\r\nContent-Type: text/html; charset=UTF-8\r\n\r\n<html><body><p>PLAYERS</p><table border=\"1\"><tr><td>Name</td><td>Won</td><td>Lost</td><td>Tied</td></tr>");

            conn.Open();

            MySqlCommand command = conn.CreateCommand();

            command.CommandText = "select Players, Won, Lost, Tied from Players";

            using (MySqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    s.Append("<tr><td>" + reader["Players"] + "</td><td>" + reader["Won"] + "</td><td>" + reader["Lost"] + "</td><td>" + reader["Tied"] + "</td></tr>"); 
                }
            }

            s.Append("</table></body></html>");

            ws.BeginSend(s.ToString(), (e, o) =>
            {
                ws.closeStringSocket();
            }, null);
        }

        private void requestOnePlayer(MySqlConnection conn, String request, StringSocket ws)
        {
            StringBuilder s = new StringBuilder("HTTP/1.1 200 OK\r\nConnection: close\r\nContent-Type: text/html; charset=UTF-8\r\n\r\n<html><body><p>GAME'S BY " + request + "</p><table border=\"1\"><tr><td>Game ID</td><td>Date and Time</td><td>Opponent</td><td>" + request + "'s Score</td><td>Opponent's Score</td></tr>");

            conn.Open();

            MySqlCommand command = conn.CreateCommand();

            command.CommandText = "select ID, DateAndTime, Player1ID, Player2ID, Player1Score, Player2Score from Games where (Player1ID = (select ID from Players where Players = @name) or Player2ID = (select ID from Players where Players = @name))";
            command.Prepare();
            command.Parameters.AddWithValue("@name", request);

            using (MySqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    using (MySqlConnection cn = new MySqlConnection(connectionString))
                    {
                        cn.Open();

                        MySqlCommand cmd = cn.CreateCommand();

                        cmd.CommandText = "select ID, Players from Players where (ID = @id1 or ID = @id2)";
                        cmd.Prepare();
                        cmd.Parameters.AddWithValue("@id1", (int)reader["Player1ID"]);
                        cmd.Parameters.AddWithValue("@id2", (int)reader["Player2ID"]);

                        String name = "";
                        int pScore = (int)reader["Player1Score"];
                        int opScore = (int)reader["Player2Score"];

                        using (MySqlDataReader rdr = cmd.ExecuteReader())
                        {
                            while (rdr.Read())
                            {
                                if (request != (String)rdr["Players"]) 
                                { 
                                    name = (String)rdr["Players"];
                                    if ((int)rdr["ID"] == (int)reader["Player1ID"])
                                    {
                                        pScore = (int)reader["Player2Score"];
                                        opScore = (int)reader["Player1Score"];
                                    }
                                }
                            }
                        }

                        s.Append("<tr><td>" + reader["ID"] + "</td><td>" + reader["DateAndTime"] + "</td><td>" + name + "</td><td>" + pScore + "</td><td>" + opScore + "</td></tr>");
                    }
                }
            }

            s.Append("</table></body></html>");

            ws.BeginSend(s.ToString(), (e, o) =>
            {
                ws.closeStringSocket();
            }, null);
        }

        private void requestGame(MySqlConnection conn, String request, StringSocket ws)
        {
            StringBuilder s = new StringBuilder("HTTP/1.1 200 OK\r\nConnection: close\r\nContent-Type: text/html; charset=UTF-8\r\n\r\n<html><body><p>GAME " + request + "</p><table border = 1><tr><td>Date and Time</td><td>Player 1</td><td>Player 2</td><td>Player 1's Score</td><td>Player 2's Score</td><td>Board</td><td>Time Limit</td></tr>");

            conn.Open();

            MySqlCommand command = conn.CreateCommand();

            command.CommandText = "select DateAndTime, Player1ID, Player2ID, Player1Score, Player2Score, Board, TimeLimit from Games where ID = @id";
            command.Prepare();
            command.Parameters.AddWithValue("@id", request);

            int p1ID = 0, p2ID = 0;

            using (MySqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    using (MySqlConnection cn = new MySqlConnection(connectionString))
                    {
                        cn.Open();

                        MySqlCommand cmd = cn.CreateCommand();

                        cmd.CommandText = "select ID, Players from Players where (ID = @id1 or ID = @id2)";
                        cmd.Prepare();
                        cmd.Parameters.AddWithValue("@id1", (int)reader["Player1ID"]);
                        cmd.Parameters.AddWithValue("@id2", (int)reader["Player2ID"]);

                        p1ID = (int)reader["Player1ID"];
                        p2ID = (int)reader["Player2ID"];

                        string p1name = "";
                        string p2name = "";

                        using (MySqlDataReader rdr = cmd.ExecuteReader())
                        {
                            while (rdr.Read())
                            {
                                if ((int)reader["Player1ID"] == (int)rdr["ID"])
                                {
                                    p1name = (string)rdr["Players"];
                                }
                                else
                                {
                                    p2name = (string)rdr["Players"];
                                }
                            }
                        }

                        s.Append("<tr><td>" + reader["DateAndTime"] + "</td><td>" + p1name + "</td><td>" + p2name + "</td><td>" + reader["Player1Score"] + "</td><td>" + reader["Player2Score"] + "</td><td><table border=\".5\">");
                        int count = 0;
                        for (int i = 0; i < 4; i++)
                        {
                            s.Append("<tr>");
                            for (int j = 0; j < 4; j++)
                            {
                                s.Append("<td>" + ((string)reader["Board"]).ToCharArray()[count] + "</td>");
                                count++;
                            }
                            s.Append("</tr>");
                        }
                        s.Append("</table>");

                        s.Append("<td>" + reader["TimeLimit"] + "</td></tr></table>");
                    }
                }
            }

            command.CommandText = "select Word, PlayerID, Legality from Words where GameID = @id and (PlayerID = @player1 or PlayerID = @player2)";
            command.Prepare();
            command.Parameters.Clear();
            command.Parameters.AddWithValue("@id", request);
            command.Parameters.AddWithValue("@player1", p1ID);
            command.Parameters.AddWithValue("@player2", p2ID);

            using (MySqlDataReader reader = command.ExecuteReader())
            {
                LinkedList<string> legalP1 = new LinkedList<string>();
                LinkedList<string> illegalP1 = new LinkedList<string>();
                LinkedList<string> legalP2 = new LinkedList<string>();
                LinkedList<string> illegalP2 = new LinkedList<string>();
                LinkedList<string> shared = new LinkedList<string>();
                
                while (reader.Read())
                {
                    if ((int)reader["PlayerID"] == p1ID)
                    {
                        if ((int)reader["Legality"] == 1) legalP1.AddLast((string)reader["Word"]);
                        else illegalP1.AddLast((string)reader["Word"]);
                    }
                    if ((int)reader["PlayerID"] == p2ID)
                    {
                        if (legalP1.Contains((string)reader["Word"])) { legalP1.Remove((string)reader["Word"]); shared.AddLast((string)reader["Word"]); continue; }
                        else if (illegalP1.Contains((string)reader["Word"])) { illegalP1.Remove((string)reader["Word"]); shared.AddLast((string)reader["Word"]); continue; }
                        if ((int)reader["Legality"] == 1) legalP2.AddLast((string)reader["Word"]);
                        else illegalP2.AddLast((string)reader["Word"]);
                    }
                }

                //s.Append("<p>" + legalP1 + "</p>" + "<p>" + illegalP1.ToString() + "</p>" + "<p>" + legalP2.ToString() + "</p>" + "<p>" + illegalP2.ToString() + "</p>" + "<p>" + shared.ToString() + "</p>");

                s.Append("<p>Player 1 Legal:</p><p>-");
                foreach (string f in legalP1)
                    s.Append(f + " ");
                s.Append("</p><p>Player 1 Illegal:</p><p>-");
                foreach (string f in illegalP1)
                    s.Append(f + " ");
                s.Append("</p><p>Player 2 Legal:</p><p>-");
                foreach (string f in legalP2)
                    s.Append(f + " ");
                s.Append("</p><p>Player 2 Illegal:</p><p>-");
                foreach (string f in illegalP2)
                    s.Append(f + " ");
                s.Append("</p><p>Shared:</p><p>-");
                foreach (string f in shared)
                    s.Append(f + " ");
                s.Append("</p>");

                s.Append("</body></html>");

                ws.BeginSend(s.ToString(), (e, o) =>
                {
                    ws.closeStringSocket();
                }, null);            
            }
        }
		
		/// <summary>
		/// Loads a dictionary to be provided to all games
		/// </summary>
		/// <param name="filePath"></param>
		private void loadDictionary(string filePath)
		{
			StreamReader streamReader = new StreamReader(filePath);
			
			string line = "";
			
			while ((line = streamReader.ReadLine()) != null)
			{
				legalWords.Add(line, true);
			}
			
			streamReader.Close();
		}
		
		/// <summary>
		/// Tells the gameServer to close
		/// </summary>
		public void CloseServer()
		{
			closeServer = true;
		}
		
		/// <summary>
		/// Sets up a gameServer with command line arguments. The gameServer is closed once return is entered
		/// NOTE - COMMAND LINE ARGUMENTS MAY DIFFER FROM MACHINE TO MACHINE IF THE DICTIONARY'S LOCATION IS NOT CONSISTENT
		/// </summary>
		/// <param name="args"></param>
		static void Main(string[] args)
		{
			//BoggleServer gameServer = new BoggleServer("60", "dictionary.txt", "ABCDEFGHIJKLMNOP");

			if (args[0] == null || args[1] == null)
			{
				return;
			}
			BoggleServer gameServer;
			if (args.Length < 3)
			{
				gameServer = new BoggleServer(args[0], args[1]);
			}
			else
			{
				gameServer = new BoggleServer(args[0], args[1], args[2]);
			}
			Console.ReadLine();
			gameServer.CloseServer();
		}
	}
}
