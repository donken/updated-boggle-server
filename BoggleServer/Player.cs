using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using CustomNetworking;

namespace ServerUtilities
{
	public class Player
	{
		/// <summary>
		/// Representation Invariant - Names must be a) unique and b) substantial ("" and " " are not valid names)
		/// The string socket will be provided by the server
		/// Words will be provided by the client
		/// </summary>
		private string name;
		private StringSocket stringSocket;
		private LinkedList<string> legalWords;
		private LinkedList<string> illegalWords;
		
		/// <summary>
		/// Create player with name "name" and string socket s
		/// </summary>
		/// <param name="name"></param>
		/// <param name="s"></param>
		public Player(string name, StringSocket s)
		{
			this.name = name;
			stringSocket = s;
			legalWords = new LinkedList<string>();
			illegalWords = new LinkedList<string>();
		}
		
		/// <summary>
		/// Set a new name for a player
		/// </summary>
		/// <param name="n"></param>
		public void setName(string n)
		{
			this.name = n;
		}
		
		/// <summary>
		/// Adds a word to the legal list
		/// </summary>
		/// <param name="word"></param>
		public void addLegalWord(string word)
		{
			legalWords.AddLast(word);
		}
		
		/// <summary>
		/// Adds a word to the illegal list
		/// </summary>
		/// <param name="word"></param>
		public void addIllegalWord(string word)
		{
			illegalWords.AddLast(word);
		}
		
		/// <summary>
		/// Returns the number of legal words played
		/// </summary>
		/// <returns></returns>
		public int getLegalWordCount()
		{
			return legalWords.Count;
		}
		
		/// <summary>
		/// Returns the number of illegal words played
		/// </summary>
		/// <returns></returns>
		public int getIllegalWordCount()
		{
			return illegalWords.Count;
		}
		
		/// <summary>
		/// Returns a string of all legal words played
		/// </summary>
		/// <returns></returns>
		public string getLegalWords()
		{
			string retVal = "";
			
			foreach (string s in legalWords)
			{
				retVal += s + " ";
			}
			
			return retVal.Trim();
		}
		
		/// <summary>
		/// Returns a string of all illegal words played
		/// </summary>
		/// <returns></returns>
		public string getIllegalWords()
		{
			string retVal = "";
			
			foreach (string s in illegalWords)
			{
				retVal += s + " ";
			}
			
			return retVal.Trim();
		}

        public LinkedList<String> getLegalWordList()
        {
            return legalWords;
        }

        public LinkedList<String> getIllegalWordList()
        {
            return illegalWords;
        }
		
		/// <summary>
		/// Returns the players name
		/// </summary>
		/// <returns></returns>
		public string getName()
		{
			return name;
		}
		
		/// <summary>
		/// Returns the players string socket
		/// </summary>
		/// <returns></returns>
		public StringSocket getStringSocket()
		{
			return stringSocket;
		}
	}
}
